<!--
SPDX-FileCopyrightText: 2016 Andrew Nayenko <relan@airpost.net>

SPDX-License-Identifier: GPL-3.0-or-later
-->

# Badge

Use the F-Droid badge on your site or README.md to indicate that your app is
available on the F-Droid main repository. For more info, see the
[documentation](https://f-droid.org/docs/Badges/)

Generation
----------

The file `1-translate-svg.py` will generate all translations based on the
source files in _badges/src/_, which is designed for the latin alphabet.
All other badges with left-to-right (LTR)
language-specific text at the top are generated from `top_ltr.svg`
(which is designed for English). Some badges have right-to-left (RTL) text.

For some languages, the text goes at the bottom. See the content of `1-translate-svg.py`
for all other variations and source designs. The SG is optimized (dropping any
font dependencies) with `2-optimize-svg.sh`. The file `3-export-png.sh` will
export a PNG file for each SVG file.

Prerequisits
------------

    sudo apt-get install inkscape
    sudo npm install -g svgo

Important
---------

The original designs are in the `src` directory, with various templates
depending on where the text needs to go:

* _src/top_ltr.svg_ left-to-right text at the top
* _src/top_rtl.svg_ right-to-left text at the top
* _src/bottom_ltr_.svg_ left-to-right text at the bottom
* _src/top_and_bottom_ltr.svg_ left-to-right text at the top and bottom
* _src/top_and_bottom_rtl.svg_ right-to-left text at the top and bottom

If needed, only change those files. All other files are generated **and**
optimized SVG. All PNG files are also always generated.


Translations
------------

You can join the translation portal for F-Droid and more specific for badges here: https://hosted.weblate.org/projects/f-droid/badge/

Designs
-------

Some specific examples are:

<img src="https://f-droid.org/badge/get-it-on.png" height="100">
<img src="https://f-droid.org/badge/get-it-on-uz.png" height="100">
<img src="https://f-droid.org/badge/get-it-on-ja.png" height="100">
<img src="https://f-droid.org/badge/get-it-on-he.png" height="100">
<img src="https://f-droid.org/badge/get-it-on-eu.png" height="100">

but you can access any locale by using `https://f-droid.org/badge/get-it-on-xx.png` or `https://f-droid.org/badge/get-it-on-xx.svg` where `xx` is the language code as seen in https://gitlab.com/fdroid/artwork/-/tree/master/badge/src
