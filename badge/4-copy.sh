#!/bin/sh -ex

# SPDX-FileCopyrightText: 2024 linsui <linsui@inbox.lv>
# SPDX-License-Identifier: GPL-3.0-or-later

for ext in "svg" "png"; do
    SRC=get-it-on-en.$ext
    cp -a $SRC get-it-on.$ext       # Default
    cp -a $SRC get-it-on-en-au.$ext # Australian English
    cp -a $SRC get-it-on-en-ca.$ext # Canadian English
    cp -a $SRC get-it-on-en-gb.$ext # British English
    cp -a $SRC get-it-on-en-us.$ext # American English

    cp get-it-on-zh-cn.$ext get-it-on-zh-hans.$ext # Simplified Chinese
    cp get-it-on-zh-tw.$ext get-it-on-zh-hant.$ext # Traditional Chinese
done
